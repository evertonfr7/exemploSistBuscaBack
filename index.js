const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(express.json());

let users = [{
    name: 'Guilherme',
    login: 'guialves77',
    img: 'http://placehold.it/800x800'
}, {
    name: 'Everton',
    login: 'evertfreit77',
    img: 'http://placehold.it/800x800'
}, {
    name: 'Iriz',
    login: 'irizaragao',
    img: 'http://placehold.it/800x800'
}, {
    name: 'Carol',
    login: 'heycarolribeiro',
    img: 'http://placehold.it/800x800'
}
]

app.get('/', (req, res) => {

    //const { search } = req.query;

    res.json({
        users
    })
});

app.listen(3333, () => {
    console.log("Server started...");
});